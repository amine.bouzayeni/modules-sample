variable "sa_name" {
  description = "Storage account name"
}

variable "container_name" {
    description = "The name of a container directory to create inside the storage account"
    
}

variable "rg_name" {
  description = ""
}

variable "location" {
  description = "value"
}

variable "environment" {
  
}
rg_name = "ks-terraform"

location = "francecentral"

public_ip_name = "vm-public-ip"

vnet_name = "vnet"

address_space = "10.0.0.0/16"

subnet_name = "subnet"

subnet_address = "10.0.0.0/24"

netint_name = "netint"

vm_name = "vm"

size = "Standard_A1_v2"

admin_username = "elyadata"

os_disk_caching = "ReadWrite"

os_storage_account_type = "Standard_LRS"

docker_username = "mbouzayeni"

docker_registry = "mbouzayeni"

docker_image = "django-app"

docker_tag = "v1.1"

port = "8235"
variable "environment" {
  description = "deployment environment"
  default = "dev"
}


variable "rg_name" {
  description = "ressource group name"
}

variable "location" {
  description = "region to deploy"
}

## public ip variables declaration

variable "public_ip_name" {
  description = "vm public ip address"
}


## vnet variables declarations

variable "vnet_name" {
  description = "virtual network name"
}

variable "address_space" {
  description = "CIDR block of the vnet"
}

variable "subnet_name" {
  description = "name of the subnet to create in the vnet"
}

variable "subnet_address" {
  description = "subnet adress range"
}

## VM module variables

variable "netint_name" {
  description = "network interface name"
}

variable "vm_name" {
  description = "virtual machine name"
}

variable "size" {
  description = "virtual machine size"
}

variable "admin_username" {
  description = "admin username"
}


variable "os_disk_caching" {
  description = "the type of caching that should be used for the internal disk"
  validation {
    condition     = contains(["None", "ReadOnly", "ReadWrite"], var.os_disk_caching)
    error_message = "The os_disk_caching variable must be one of the following values: None, ReadOnly, ReadWrite."
  }
}

variable "os_storage_account_type" {
  description = "Storage account type. Affects durability availability and other parameters"
  validation {
    condition     = contains(["Standard_LRS", "StandardSSD_LRS", "Premium_LRS", "StandardSSD_ZRS", "Premium_ZRS"], var.os_storage_account_type)
    error_message = "The os_storage_account_type variable must be one of the following values: Standard_LRS, StandardSSD_LRS, Premium_LRS, StandardSSD_ZRS, Premium_ZRS."
  }
}

variable "docker_username" {
  description = "docker username"
}

variable "docker_registry" {
  
}

variable "docker_image" {
  
}

variable "docker_tag" {
  
}

variable "port" {
  
}

variable "admin_private_key_path" {
  
}
module "vnet" {
  source = "../modules/vnet"

  vnet_name = "${var.environment}-${var.vnet_name}"
  location = var.location
  rg_name = var.rg_name
  address_space = var.address_space
  subnet_name = "${var.environment}-${var.subnet_name}"
  subnet_address = "${var.subnet_address}"
  environment = var.environment
}

output "subnet_id" {
  value = module.vnet.subnet_id
}

module "vm" {
  source = "../modules/vm"

  public_ip_name = "${var.environment}-${var.public_ip_name}"
  netint_name = "${var.environment}-${var.netint_name}"
  location = var.location
  rg_name = var.rg_name
  subnet_id = module.vnet.subnet_id
  vm_name = "${var.environment}-${var.vm_name}"
  size = var.size
  admin_username = var.admin_username
  admin_pub_key_path = "${path.root}/.ssh/id_rsa.pub"
  os_disk_caching = var.os_disk_caching
  os_storage_account_type = var.os_storage_account_type
  environment = var.environment
}

output "public_ip" {
  value = module.vm.public_ip
}

module "ansible-installer" {
  source = "../modules/ansible-installer"

  depends_on = [ module.vm ]

  vm_name = var.vm_name
  ip = module.vm.public_ip
  admin_username = var.admin_username
  admin_private_key_path = var.admin_private_key_path
  docker_username = var.docker_username
  port = var.port
}

variable "public_ip_name" {
  description = "vm public ip address"
}

variable "environment" {
  description = "deployment environment"
}

variable "netint_name" {
  description = "network interface name"
}

variable "subnet_id" {
  description = "subnet id in which the network interface is attached"
}

variable "vm_name" {
  description = "virtual machine name"
}

variable "rg_name" {
  description = "resource group name"
}

variable "location" {
  description = "region to deploy the resource"
}

variable "size" {
  description = "virtual machine size"
}

variable "admin_username" {
  description = "admin username"
}

variable "admin_pub_key_path" {
  description = "adminuser public key path for ssh authentication"
}

variable "os_disk_caching" {
  description = "the type of caching that should be used for the internal disk"
  validation {
    condition     = contains(["None", "ReadOnly", "ReadWrite"], var.os_disk_caching)
    error_message = "The os_disk_caching variable must be one of the following values: None, ReadOnly, ReadWrite."
  }
}

variable "os_storage_account_type" {
  description = "Storage account type. Affects durability availability and other parameters"
  validation {
    condition     = contains(["Standard_LRS", "StandardSSD_LRS", "Premium_LRS", "StandardSSD_ZRS", "Premium_ZRS"], var.os_storage_account_type)
    error_message = "The os_storage_account_type variable must be one of the following values: Standard_LRS, StandardSSD_LRS, Premium_LRS, StandardSSD_ZRS, Premium_ZRS."
  }
}


resource "azurerm_public_ip" "public_ip" {
  name                = var.public_ip_name
  resource_group_name = var.rg_name
  location            = var.location
  allocation_method   = "Static"

  tags = {
    environment = var.environment
  }

}


resource "azurerm_network_interface" "netint" {
  name                = var.netint_name
  location            = var.location
  resource_group_name = var.rg_name
  
  
  ip_configuration {
    name                          = "internal_config"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          =  azurerm_public_ip.public_ip.id
  }
  
  tags = {
    environment = var.environment
  }
}

resource "azurerm_network_security_group" "security_group" {
  name                = "acceptanceTestSecurityGroup1"
  location            = var.location
  resource_group_name = var.rg_name

  security_rule {
    name                       = "appRule"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["22", "8235"]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Production"
  }
}

resource "azurerm_network_interface_security_group_association" "netint_sec" {
  network_interface_id      = azurerm_network_interface.netint.id
  network_security_group_id = azurerm_network_security_group.security_group.id
}

resource "azurerm_linux_virtual_machine" "vm" {
  name                = var.vm_name
  resource_group_name = var.rg_name
  location            = var.location
  size                = var.size
  admin_username      = var.admin_username
  network_interface_ids = [
    azurerm_network_interface.netint.id,
  ]

  admin_ssh_key {
    username   = var.admin_username
    public_key = file(var.admin_pub_key_path)
  }

  os_disk {
    caching              = var.os_disk_caching
    storage_account_type = var.os_storage_account_type
  }

  source_image_reference { # Ubuntu 22.04 LTS
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }

  tags = {
    environment = var.environment
  }
}


# resource "null_resource" "deploy-app" {


# depends_on = [ azurerm_linux_virtual_machine.vm ]

#   provisioner "remote-exec" {
#       inline = [
#       "sudo apt-get update",
#       "sudo apt-get install ca-certificates curl",
#       "sudo install -m 0755 -d /etc/apt/keyrings",
#       "sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc",
#       "sudo chmod a+r /etc/apt/keyrings/docker.asc",
#       "echo \"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu $(. /etc/os-release && echo \"$VERSION_CODENAME\") stable\" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null",
#       "sudo apt-get update",
#       "sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y",
#       "sudo usermod -aG docker $USER && newgrp docker",
#       "docker login -u ${var.docker_username} -p ${var.docker_password}",
#       "docker pull ${var.docker_registry}/${var.docker_image}:${var.docker_tag}",
#       "docker run --name django-app -p ${var.port}:${var.port} ${var.docker_registry}/${var.docker_image}:${var.docker_tag}"

#     ]
#     }
    
#   connection {
#       type        = "ssh"
#       user        = var.admin_username
#       private_key = file("${path.root}/.ssh/id_rsa")
#       host        = azurerm_public_ip.public_ip.ip_address
#       #host = data.azurerm_public_ip.publicip[count.index].fqdn
#       #host        = azurerm_public_ip.test-publicIP-VM[count.index].ip_address
#       timeout        = "2m"
#     }
# }

# resource "azurerm_virtual_machine_extension" "init-script" {
#   name                 = "init-script"
#   virtual_machine_id   = azurerm_linux_virtual_machine.vm.id
#   publisher            = "Microsoft.Azure.Extensions"
#   type                 = "CustomScript"
#   type_handler_version = "2.0"

#   settings = <<PROTECTED_SETTINGS
# {
#     "script": "${base64encode(file("${path.module}/init-script.sh"))}"
# }
# PROTECTED_SETTINGS


#   tags = {
#     environment = var.environment
#   }
# }
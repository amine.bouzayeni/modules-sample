variable "vm_name" {
  description = "virtual machine name"
}

variable "ip" {
  
}

variable "admin_username" {
  
}

variable "admin_private_key_path" {
  
}

variable "docker_username" {
  
}

variable "port" {
  
}
resource "local_file" "tf_ansible_inventory_file" {
  content = <<-DOC
    [${var.vm_name}]
    ${var.vm_name} ansible_host=${var.ip} ansible_user=${var.admin_username} ansible_python_interpreter=/usr/bin/python3 ansible_ssh_private_key_file=${var.admin_private_key_path}
    DOC
  filename = "./ansible-scripts/inventory"
}

resource "null_resource" "ansible_exec" {
  triggers = {
    always_run = "${timestamp()}"
  }
  provisioner "local-exec" {
  command = "ansible-playbook -e \"admin_username=${var.admin_username} docker_username=${var.docker_username} port=${var.port} ANSIBLE_HOST_KEY_CHECKING=False\" --ask-vault-pass -i ./ansible-scripts/inventory ./ansible-scripts/setup.yml"
  
}
}
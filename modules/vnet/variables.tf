variable "vnet_name" {
  description = "virtual network name"
}

variable "location" {
  description = "region to deploy to"
}

variable "rg_name" {
  description = "ressource group name"
}

variable "address_space" {
  description = "CIDR block of the vnet"
}

variable "subnet_name" {
  description = "name of the subnet to create in the vnet"
}

variable "subnet_address" {
  description = "subnet adress range"
}

variable "environment" {
  description = "deployment environment" 
}


# Check if it propagate to subnet also
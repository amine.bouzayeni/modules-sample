variable "sa_name" {
  description = "Storage account name"
}

variable "rg_name" {
  description = "ressource group name in which the storage account will be created"
}

variable "location" {
  description = "Storage account location"
}

variable "account_tier" {
  description = "Storage account account tier"
  validation {
    condition     = can(regex("^(Standard|Premium)$", var.account_tier))
    error_message = "Invalid account tier selected, only allowed values are: 'Standard', 'Premium'"
  }
  default = "Standard"
}

variable "account_replication_type" {
  description = "The type of replication to use for this storage account. Chaniging this forces a new resource creation"
  validation {
    condition     = can(regex("^(LRS|GRS|RAGRS|ZRS|GZRS|RAGZRS)$", var.account_replication_type))
    error_message = "Invalid account tier selected, only allowed values are: 'LRS', 'GRS', 'RAGRS', 'ZRS', 'GZRS', 'RAGZRS'"
  }
  default = "GRS"
}

variable "container_name" {
    description = "The name of a container directory to create inside the storage account"
    
}

variable "container_access_type" {
  description = "The Access Level configured for this Container."
  validation {
    condition = can(regex("^(blob|container|private)$", var.container_access_type))
    error_message = "Invalid container access type selected, only allowed values are: 'blob', 'container', 'private'"
  }
  default = "private"
}

variable "environment" {
  
}
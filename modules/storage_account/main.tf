resource "azurerm_storage_account" "backend-tf" {
  name                     = "backendelyadata${var.sa_name}"
  resource_group_name      = var.rg_name
  location                 = var.location
  #Blob Storage (including Data Lake Storage1), Queue Storage, Table Storage, and Azure Files
  account_tier             = var.account_tier
  #GRS provides additional redundancy for data storage compared to LRS or ZRS. In addition to the three copies of data stored in one region
  account_replication_type = var.account_replication_type

  public_network_access_enabled = false

  tags = {
    environment = var.environment
  }
}

resource "azurerm_storage_container" "container-tf" {
  name                  = var.container_name
  storage_account_name  = azurerm_storage_account.backend-tf.name
  container_access_type = var.container_access_type
}

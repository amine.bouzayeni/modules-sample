module "resource_group" {
  source = "../modules/resource_group"
  
  rg_name = var.rg_name
  location = var.location
}


module "vnet" {
  source = "../modules/vnet"

  vnet_name = var.vnet_name
  location = var.location
  rg_name = var.rg_name
  address_space = var.address_space
  subnet_name = var.subnet_name
  subnet_address = var.subnet_address
  vnet_tag = var.vnet_tag
}

output "subnet_id" {
  value = module.vnet.subnet_id
}

module "vm" {
  source = "../modules/vm"

  netint_name = var.netint_name
  location = var.location
  rg_name = var.rg_name
  subnet_id = module.vnet.subnet_id
  vm_name = var.vm_name
  size = var.size
  admin_username = var.admin_username
  admin_pub_key_path = "${path.root}/.ssh/id_rsa.pub"
  os_disk_caching = var.os_disk_caching
  os_storage_account_type = var.os_storage_account_type
}
rg_name = "prod"

location = "France Centrale"

vnet_name = "prod-vnet"

address_space = "10.1.0.0/24"

subnet_name = "prod-subnet"

subnet_address = "10.1.1.0/16"

vnet_tag = "prod"

netint_name = "prod-netint"

vm_name = "prod-vm"

size = "Standard_A1_v2"

admin_username = "elyadata"

os_disk_caching = "ReadWrite"

os_storage_account_type = "Standard_LRS"